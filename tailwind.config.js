/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'inputColor': '#003153',
      }
    },
  },
  plugins: [require("@tailwindcss/forms")],
}
