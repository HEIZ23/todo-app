import React from 'react';
import Check from "./item/Check";
import Edit from "./item/Edit";
import Trash from "./item/Trash";


const Todo = ({todo, changeTodo, removeTodo}) => {
    return (<div
        className="border-3 m-2 p-2 flex justify-between shadow-sm rounded-2xl bg-[#005e99] text-white mb-2 border-2">
        <button onClick={() => changeTodo(todo.id)}>
            <Check isCompleted={todo.isCompleted}/>
        </button>

        <span className={`my-auto ${todo.isCompleted ? 'line-through' : ''}  `}>{todo.title}</span>
        <div className="flex my-1 mr-2">
            <button>
                <Edit/>
            </button>
            <button onClick={()=> removeTodo(todo.id)}>
                <Trash/>
            </button>
        </div>

    </div>)

}

export default Todo;