import React, {useState} from 'react';
import Todo from './Todo'
import Plus from "./item/Plus";

const data = [
    {
        id: 1,
        title: 'Выучить слова',
        isCompleted: false
    },
    {
        id: 2,
        title: 'Пойти гулять',
        isCompleted: false
    },
    {
        id: 3,
        title: 'Помыть посуду',
        isCompleted: false
    },
    {
        id: 4,
        title: 'Попрогать',
        isCompleted: false
    },
    {
        id: 5,
        title: 'Сходить в магазин',
        isCompleted: false
    },

]


const Main = () => {
    const [todos, setTodos] = useState(data);
    const [titleTodo, setTitleTodo] = useState('');

    const changeTodo = id => {
        const copy = [...todos]
        const current = copy.find(todo => todo.id === id)
        current.isCompleted = !current.isCompleted
        setTodos(copy)
    }

    const removeTodo = id => setTodos([...todos].filter(todo => todo.id !== id))

    const addTodo = title => {
        let id = [...todos].length + 1
        setTodos([{
            id: id,
            title,
            isCompleted: false
        }, ...todos])

    }


    return (
        <div className="Main mx-auto bg-white max-w-lg rounded-2xl my-2 max-h-fit p-3">
            <div className="flex text-center justify-between py-5 mx-7">
                <input type="text"
                       className='border-b-4  border-0 outline-0 shadow-sm w-4/5 h-10 bg-gray-100 focus:ring-0'
                       value={titleTodo}
                       onChange={(event) => setTitleTodo(event.target.value)}/>
                <button onClick={() => addTodo(titleTodo)}>
                    <Plus/>
                </button>
            </div>
            {todos.map(todo => (<Todo key={todo.id} todo={todo} changeTodo={changeTodo} removeTodo={removeTodo}/>))}
        </div>

    )
}

export default Main;