import React from 'react';
import {BsTrashFill} from "react-icons/bs";

const Trash = () => {
    return (
        <div className="rounded-full w-10 h-10 mx-1 cursor-pointer bg-red-600 flex items-center justify-center hover:bg-red-800">
            <BsTrashFill size={26} className=""/>
        </div>
    )
}

export default Trash;