import React from 'react';
import {MdEdit} from "react-icons/md";

const Edit = () => {
 return (
     <div className={`rounded-full w-10 h-10 mx-1 cursor-pointer bg-gray-600 flex items-center justify-center hover:bg-gray-700`}>
         <MdEdit size={26} className=""/>
     </div>
 )
}

export default Edit;