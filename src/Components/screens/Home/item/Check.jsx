import React from 'react';
import {BiCheck} from "react-icons/bi";

const Check = ({isCompleted}) => {
    return (

        <div className={` focus:bg-green-600 ${isCompleted ? 'bg-green-600' : 'bg-white'} rounded-full w-10 h-10 ml-3 cursor-pointer flex items-center justify-center`}>
            {isCompleted &&
                <BiCheck size={36} className="text-white"/>
            }

        </div>
    )
}

export default Check