import React from 'react';
import {BsPlusLg} from "react-icons/bs";

const Plus = () => {
    return (
        <div
            className=" rounded-full w-10 h-10 cursor-pointer bg-red-600 flex items-center justify-center hover:bg-red-800 ">
            <BsPlusLg className="text-white"/>
        </div>
    )
}

export default Plus;